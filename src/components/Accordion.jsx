import AccordionGroup from "@mui/joy/AccordionGroup";
import Accordion from "@mui/joy/Accordion";
import AccordionDetails, {
  accordionDetailsClasses,
} from "@mui/joy/AccordionDetails";
import AccordionSummary, {
  accordionSummaryClasses,
} from "@mui/joy/AccordionSummary";
import Switch from "@mui/joy/Switch";
import Stack from "@mui/joy/Stack";
import Typography from "@mui/joy/Typography";
import Avatar from "@mui/joy/Avatar";
import ListItemContent from "@mui/joy/ListItemContent";

import { Check, Close } from "@mui/icons-material";
import { SvgIcon } from "@mui/joy";

export default function AccordionCustom({ data, estimatedReqAvg }) {
  const { id, steps, duration, co2, passed } = data;
  let number = estimatedReqAvg * co2;
  let formattedNumber = parseFloat(number.toFixed(2));
  return (
    <Accordion>
      <AccordionSummary>
        {passed ? (
          <Avatar color="success">
            <SvgIcon size="lg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <g fill="#1e6726">
                  <path
                    d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10z"
                    opacity="0.5"
                  ></path>
                  <path d="M16.03 8.97a.75.75 0 010 1.06l-5 5a.75.75 0 01-1.06 0l-2-2a.75.75 0 111.06-1.06l1.47 1.47 2.235-2.236L14.97 8.97a.75.75 0 011.06 0z"></path>
                </g>
              </svg>
            </SvgIcon>
          </Avatar>
        ) : (
          <Avatar color="danger">
            <SvgIcon size="lg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <g fill="#730d0d">
                  <path
                    d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10z"
                    opacity="0.5"
                  ></path>
                  <path d="M8.97 8.97a.75.75 0 011.06 0L12 10.94l1.97-1.97a.75.75 0 111.06 1.06L13.06 12l1.97 1.97a.75.75 0 01-1.06 1.06L12 13.06l-1.97 1.97a.75.75 0 01-1.06-1.06L10.94 12l-1.97-1.97a.75.75 0 010-1.06z"></path>
                </g>
              </svg>
            </SvgIcon>
          </Avatar>
        )}
        <ListItemContent>
          <Typography level="title-md" color={passed ? "success" : "danger"}>
            {id}
          </Typography>
          <Stack direction="row" spacing={2}>
            <Typography level="body-sm">Total Steps: {steps.length}</Typography>
            <Typography level="body-sm">
              Total Duration: {duration}ms
            </Typography>
            <Typography level="body-sm">
              Total Carbon: {formattedNumber}g
            </Typography>
          </Stack>
        </ListItemContent>
      </AccordionSummary>
      <AccordionDetails>
        <Stack direction='column' spacing={2}>
        {steps?.map((step) => {
          const { name, passed, duration, co2 } = step;
          let number = estimatedReqAvg * co2;
          let formattedNumber = parseFloat(number.toFixed(2));
          return (
            <Stack
              spacing={1.5}
              direction="row"
              alignItems="center"
              justifyContent="space-between"
            >
              {passed ? (
                <Stack direction="row" alignItems="center">
                  <Check fontSize="medium" sx={{ mx: 1 }} color="success" />
                  <Typography variant="outlined" color="success">{name}</Typography>
                </Stack>
              ) : (
                <Stack direction="row" alignItems="center">
                  <Close fontSize="medium" sx={{ mx: 1 }} color="error" />
                  <Typography variant="outlined" color="danger">{name}</Typography>
                </Stack>
              )}
              <Stack direction="row" spacing={2}>
                <Typography level="body-sm">duration: {duration}ms</Typography>
                <Typography level="body-sm">carbon: {formattedNumber}g</Typography>
              </Stack>
            </Stack>
          );
        })}
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
}
