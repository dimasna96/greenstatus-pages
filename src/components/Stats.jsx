import Button from "@mui/joy/Button";
import Card from "@mui/joy/Card";
import CardContent from "@mui/joy/CardContent";
import CardActions from "@mui/joy/CardActions";
import CircularProgress from "@mui/joy/CircularProgress";
import Typography from "@mui/joy/Typography";
import SvgIcon from "@mui/joy/SvgIcon";

export default function Stats({ info, data }) {
  return (
    <Card variant="solid" sx={{ backgroundColor: "white", color: "black" }}>
      <CardContent orientation="horizontal">
        {info === "passed" ? (
          <>
            <CircularProgress size="lg" determinate value={100} color="success">
              <SvgIcon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  stroke="#0d7239"
                  viewBox="0 0 24 24"
                >
                  <g>
                    <g>
                      <path
                        stroke="#0d7239"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M6 12l4.243 4.243 8.484-8.486"
                      ></path>
                    </g>
                  </g>
                </svg>
              </SvgIcon>
            </CircularProgress>
            <CardContent>
              <Typography level="body-md" sx={{ color: "#0d7239" }}>
                Total Passed
              </Typography>
              <Typography level="h2" color="success">
                {data.passedTest} <span style={{ fontSize: 18, color: "black" }}>/ {data.totalTest}</span>
              </Typography>
            </CardContent>
          </>
        ) : info === "failed" ? (
          <>
            <CircularProgress size="lg" determinate value={100} color="danger">
              <SvgIcon>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                >
                  <path
                    fill="#ab1212"
                    fillRule="evenodd"
                    d="M16.95 8.464a1 1 0 10-1.414-1.414L12 10.586 8.465 7.05A1 1 0 007.05 8.464L10.586 12 7.05 15.536a1 1 0 101.415 1.414L12 13.414l3.536 3.536a1 1 0 101.414-1.414L13.414 12l3.536-3.536z"
                    clipRule="evenodd"
                  ></path>
                </svg>
              </SvgIcon>
            </CircularProgress>
            <CardContent>
              <Typography level="body-md" sx={{ color: "#ab1212" }}>
                Total Failed
              </Typography>
              <Typography level="h2" color="danger">
                {data.failedTest} <span style={{ fontSize: 18, color: "black" }}>/ {data.totalTest}</span>
              </Typography>
            </CardContent>
          </>
        ) : (
          <>
            <CircularProgress size="lg" determinate value={100} color="warning">
              <SvgIcon size="lg">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="#925806"
                  viewBox="0 0 512 512"
                >
                  <g>
                    <g>
                      <path d="M198.91 155.31h8.26a30.81 30.81 0 0030.77-30.77v-.34h-18v.34a12.79 12.79 0 01-12.77 12.77h-8.26a12.79 12.79 0 01-12.77-12.77V92.76A12.79 12.79 0 01198.91 80h8.26a12.79 12.79 0 0112.77 12.77h18A30.81 30.81 0 00207.17 62h-8.26a30.8 30.8 0 00-30.77 30.77v31.78a30.8 30.8 0 0030.77 30.76zM283.47 155.31a34.94 34.94 0 0034.9-34.9V96.89a34.9 34.9 0 10-69.8 0v23.52a34.93 34.93 0 0034.9 34.9zm-16.9-58.42a16.9 16.9 0 1133.8 0v23.52a16.9 16.9 0 11-33.8 0zM362.8 155.25l-17.13 5.44a20.44 20.44 0 006.18 39.92h27.4v-18h-27.4a2.44 2.44 0 01-.73-4.76l17.13-5.44a21.64 21.64 0 0015.14-20.69v-2.13a21.74 21.74 0 00-21.72-21.71h-8.54a21.74 21.74 0 00-21.71 21.71v2.63h18v-2.63a3.72 3.72 0 013.71-3.71h8.54a3.72 3.72 0 013.72 3.71v2.13a3.69 3.69 0 01-2.59 3.53zM227.17 197.14a69.65 69.65 0 0166.56 49.92 25.71 25.71 0 0025 18.6h.14a53.1 53.1 0 0153.08 54 51.68 51.68 0 01-5.07 21.34 59.37 59.37 0 0116.61 7 69.36 69.36 0 006.44-28 71.1 71.1 0 00-71.08-72.3h-.18A7.86 7.86 0 01311 242a87.33 87.33 0 00-167.69 0 7.87 7.87 0 01-7.68 5.62h-.16A71.1 71.1 0 0064.41 320c.64 38.52 33 69.87 72.09 69.87h28.64a59.43 59.43 0 015.92-18H136.5c-29.35 0-53.62-23.41-54.1-52.17a53.1 53.1 0 0153.08-54h.18a26.81 26.81 0 005.26-.53 37.07 37.07 0 0110.05 25l20.33-.58a56.61 56.61 0 00-13.56-36 26 26 0 002.86-6.42 69.66 69.66 0 0166.57-50.03z"></path>
                      <path d="M354.22 356.33a32.91 32.91 0 00-13.34 26.05l-18-.51a50.25 50.25 0 0117-37 54.55 54.55 0 00-103.78 3 11.76 11.76 0 01-11.38 8.44h-.1a43.23 43.23 0 00-43.23 44c.39 23.71 20.23 42.5 43.94 42.5h126.25c23.71 0 43.55-18.79 43.95-42.5a43.23 43.23 0 00-41.31-43.98z"></path>
                    </g>
                  </g>
                </svg>
              </SvgIcon>
            </CircularProgress>
            <CardContent>
              <Typography level="body-md" sx={{ color: "#925806" }}>
                Estimated Emission
              </Typography>
              <Typography level="h2" color="warning">
                {data} <span style={{ fontSize: 18, color: "black" }}>g</span>
              </Typography>
            </CardContent>
          </>
        )}
      </CardContent>
      {/* <CardActions>
        <Button variant="soft" size="sm">
          Add to Watchlist
        </Button>
        <Button variant="solid" size="sm">
          See breakdown
        </Button>
      </CardActions> */}
    </Card>
  );
}
