import logo from "./logo.svg";
import {
  Box,
  Card,
  Stack,
  Grid,
  Button,
  CardContent,
  CardActions,
  CircularProgress,
  Typography,
  SvgIcon,
  Link,
} from "@mui/joy";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";

import "./App.css";
import Stats from "./components/Stats";
import AccordionCustom from "./components/Accordion";
import AccordionGroup from "@mui/joy/AccordionGroup";
import AccordionDetails, {
  accordionDetailsClasses,
} from "@mui/joy/AccordionDetails";
import AccordionSummary, {
  accordionSummaryClasses,
} from "@mui/joy/AccordionSummary";
import { useEffect, useState } from "react";

function App() {
  const [data, setData] = useState({});
  const [estEmission, setEstEmission] = useState(0);
  const [totalTest, setTotalTest] = useState(0);
  const [passedTest, setPassedTest] = useState(0);
  const [failedTest, setFailedTest] = useState(0);

  useEffect(() => {
    fetch("data.json")
      .then((res) => res.json())
      .then((body) => {
        setData(body);

        //estEmission
        let number = body?.estimatedReqAvg * body?.co2;
        let formattedNumber = parseFloat(number.toFixed(2));
        setEstEmission(formattedNumber);

        //totalTest
        let tests = body?.tests;
        setTotalTest(tests.length);

        let passedTest = tests.filter((test) => test.passed === true);
        setPassedTest(passedTest);

        let failedTest = tests.filter((test) => test.passed === false);
        setFailedTest(failedTest);
      });
  }, []);

  useEffect(() => {
    console.log(data);
  }, [data]);
  return (
    <div className="App">
      <Stack
        direction="column"
        justifyContent="space-evenly"
        alignItems="stretch"
        spacing={0}
      >
        <Box height={276} width="100%" bgcolor={"black"} color="white">
          <Stack
            direction="row"
            justifyContent="space-between"
            alignItems="center"
            spacing={2}
            paddingX={4}
            marginTop={8}
          >
            <Stack direction='row' alignItems='center' spacing={0.2}>
              <SvgIcon size="lg">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="200"
                  height="200"
                  fill="#b0ffb0"
                  version="1.1"
                  viewBox="0 0 379 379"
                  xmlSpace="preserve"
                >
                  <path d="M342.542 147.704c-7.415-17.9-22.547-47.93-40.588-47.93-4.761 0-10.99 3.969-12.787 5.187-2.127 1.442-3.239 4.202-2.706 6.712.067.317 6.743 31.932 8.147 46.828 4.161 44.158-3.688 76.499-23.328 96.142-3.925 3.924-11.349 10.388-11.42 10.451-1.5 1.306-2.217 3.185-1.919 5.087.298 1.903 1.567 3.436 3.4 4.223.713.305 13.889 5.785 30.673 8.323a7.79 7.79 0 00-.249 1.932V351h-88v-84.101c21.182-3.392 38.714-14.535 49.277-31.572 22.965-37.035 3.935-147.923-27.03-203.55C214.267 10.68 202.074 0 189.769 0h-.006c-12.191 0-24.309 10.722-36.013 31.747-31.026 55.734-50.181 166.609-27.263 203.571 13.299 21.447 34.021 28.994 49.278 31.507V351h-88v-66.636c0-.721-.102-1.413-.285-2.066 15.031-2.588 26.404-7.246 27.057-7.523 1.835-.779 3.111-2.163 3.412-4.069.301-1.907-.418-3.827-1.925-5.136-.078-.067-7.854-6.822-11.946-10.913C85.92 236.499 79.81 204.132 85.4 155.715c1.568-13.576 7.157-41.544 7.214-41.826.479-2.394-.49-5.026-2.411-6.529-2.281-1.786-10.158-7.641-15.77-7.641-15.563 0-28.099 22.321-35.875 41.092C21.49 182.022 9.104 250.816 27.71 269.422c9.076 9.076 21.545 13.819 37.091 14.487a8.052 8.052 0 00-.035.75V351H40.101c-4.518 0-8.335 3.614-8.335 7.892v11.864c0 4.392 3.895 8.244 8.335 8.244h299.727c4.303 0 7.938-3.775 7.938-8.244v-11.864c0-4.425-3.486-7.892-7.938-7.892h-25.063v-66.636c0-.211-.013-.418-.028-.625 14.116-1.105 25.515-5.815 33.939-14.24 20.381-20.379 12.05-77.893-6.134-121.795z"></path>
                </svg>
              </SvgIcon>
              <Typography
                sx={{ color: "#b0ffb0", padding: 0 }}
                level="h2"
                variant="plain"
              >
                GreenStatus
              </Typography>
            </Stack>
            <Stack direction="row" spacing={2} alignItems="center">
              <Link
                sx={{ color: "white", textDecoration: "underline" }}
                href="https://gitlab.com/dimasna96/greenstatus.git"
              >
                Get this on GitLab
              </Link>
              <Button
                endDecorator={<KeyboardArrowRight />}
                color="success"
                onClick={() =>
                  (window.location = `https://www.wren.co/offset-anything?amount=${
                    estEmission * 1e-6
                  }&unit=ton`)
                }
              >
                Offset {estEmission}g Carbon
              </Button>
            </Stack>
          </Stack>
        </Box>
        <Box paddingX={4} marginTop={-17} marginBottom={2}>
          <Grid container spacing={2}>
            <Grid xs={4}>
              <Stats
                info="passed"
                data={{ passedTest: passedTest.length, totalTest }}
              />
            </Grid>
            <Grid xs={4}>
              <Stats
                info="failed"
                data={{ failedTest: failedTest.length, totalTest }}
              />
            </Grid>
            <Grid xs={4}>
              <Stats info="carbon" data={estEmission} />
            </Grid>
          </Grid>
        </Box>
        <Box paddingX={4}>
          <Card
            sx={{ backgroundColor: "white", boxShadow: "lg" }}
            orientation="horizontal"
            size="lg"
            variant="solid"
          >
            <AccordionGroup
              variant="plain"
              transition="0.2s"
              sx={{
                borderRadius: "md",
                [`& .${accordionDetailsClasses.content}.${accordionDetailsClasses.expanded}`]:
                  {
                    paddingBlock: "1rem",
                  },
                [`& .${accordionSummaryClasses.button}`]: {
                  paddingBlock: "1rem",
                },
              }}
            >
              {data?.tests?.map((test) => {
                return (
                  <AccordionCustom
                    data={test}
                    estimatedReqAvg={data?.estimatedReqAvg}
                  />
                );
              })}
            </AccordionGroup>
          </Card>
        </Box>
      </Stack>
    </div>
  );
}

export default App;
